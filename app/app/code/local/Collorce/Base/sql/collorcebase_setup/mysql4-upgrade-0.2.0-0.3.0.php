<?php

$installer = $this;
$installer->startSetup();

Mage::register('isSecureArea', 1);
Mage::app()->setUpdateMode(false);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$cmsPages = array(
	array(
		'title' => 'Категории',
		'content_heading' => 'Категории',
		'content' => "<h2>Категории</h2>",
		'root_template' => 'one_column',
		'identifier' => 'categories',
		'stores' => array(0),
	),
	array(
		'title' => 'О нас',
		'content_heading' => 'О нас',
		'content' => "<h2>О нас</h2>",
		'root_template' => 'one_column',
		'identifier' => 'about-us',
		'stores' => array(0),
	),
	array(
		'title' => 'Оплата и доставка',
		'content_heading' => 'Оплата и доставка',
		'content' => "<h2>Оплата и доставка</h2>",
		'root_template' => 'one_column',
		'identifier' => 'payment-and-delivery',
		'stores' => array(0),
	),
	array(
		'title' => 'Новости',
		'content_heading' => 'Новости',
		'content' => "<h2>Новости</h2>",
		'root_template' => 'one_column',
		'identifier' => 'blog-news',
		'stores' => array(0),
	),
	array(
		'title' => 'Форум',
		'content_heading' => 'Форум',
		'content' => "<h2>Форум</h2>",
		'root_template' => 'one_column',
		'identifier' => 'forum',
		'stores' => array(0),
	),
	array(
		'title' => 'Контакты',
		'content_heading' => 'Контакты',
		'content' => "<h2>Контакты</h2>",
		'root_template' => 'one_column',
		'identifier' => 'contacts',
		'stores' => array(0),
	),
);

foreach ( $cmsPages as $cmsPage ) {
	Mage::getModel('cms/page')->setData($cmsPage)->save();
}

$page = Mage::getModel('cms/page')->load(2);

$layoutUpdateXmlForHomePage = <<<"LAYOUT"
<reference name="content">
    <block type="freaks_products/region" name="products.special" template="freaks/products.phtml" after="-">
        <action method="setType"><type>special</type></action>
        <action method="setProductsCount"><limit>6</limit></action>
    </block>
    <block type="freaks_products/region" name="products.bestseller" template="freaks/products.phtml" after="products.special">
        <action method="setType"><type>bestseller</type></action>
        <action method="setProductsCount"><limit>6</limit></action>
    </block>
    <block type="freaks_products/region" name="products.new" template="freaks/products.phtml" after="products.bestseller">
        <action method="setType"><type>new</type></action>
        <action method="setProductsCount"><limit>6</limit></action>
    </block>
    <!--<block type="freaks_products/region" name="products.last" template="freaks/products.phtml" after="products.new">
        <action method="setType"><type>last</type></action>
        <action method="setProductsCount"><limit>6</limit></action>
    </block>
    <block type="freaks_products/region" name="products.featured" template="freaks/featured_products.phtml" after="products.last">
        <action method="setProductsCount"><limit>6</limit></action>
    </block>-->
</reference>
LAYOUT;

$page->setTitle('Главная')
	->setContent('{{block type="core/template" name="nav.visual" template="navigation/visual.phtml"}}')
	->setLayoutUpdateXml($layoutUpdateXmlForHomePage)
	->setRootTemplate('one_column')
	->save();

$installer->endSetup();
