<?php

$installer = $this;
$installer->startSetup();

Mage::register('isSecureArea', 1);
Mage::app()->setUpdateMode(false);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$db = Mage::getSingleton('core/resource')->getConnection('core_read');

$core_store_group_table = $installer->getTable('core_store_group');
$core_website_table = $installer->getTable('core_website');
$core_store_table = $installer->getTable('core_store');
$sql = <<<"SQL"
SELECT g.root_category_id, s.store_id
 FROM {$core_store_group_table}
 g LEFT JOIN {$core_website_table}
 w ON w.default_group_id = g.group_id
 LEFT JOIN {$core_store_table}
 s ON s.group_id = g.group_id
SQL;

$rootIds = $db->query($sql)->fetchAll();

$categories = array(
	array(
		'urlKey' => 'tiles',
		'name' => 'Плитка',
		'title' => 'Плитка',
		'subCategories' => array(
			array(
				'urlKey' => 'porcelain-tiles',
				'name' => 'Керамогранит',
				'title' => 'Керамогранит',
			),
			array(
				'urlKey' => 'mosaic',
				'name' => 'Мозаика',
				'title' => 'Мозаика',
			),
			array(
				'urlKey' => 'tiles-for-wood',
				'name' => 'Плитка под дерево',
				'title' => 'Плитка под дерево',
			),
			array(
				'urlKey' => 'clay-tiles',
				'name' => 'Фасадная плитка',
				'title' => 'Фасадная плитка',
			),
			array(
				'urlKey' => 'clinker',
				'name' => 'Клинкер',
				'title' => 'Клинкер',
			),
			array(
				'urlKey' => 'for-a-kitchen',
				'name' => 'Для кухни',
				'title' => 'Для кухни',
			),
			array(
				'urlKey' => 'for-a-floor',
				'name' => 'Для пола',
				'title' => 'Для пола',
			),
			array(
				'urlKey' => 'steps',
				'name' => 'Ступени',
				'title' => 'Ступени',
			),
		),
	),
	array(
		'urlKey' => 'sanitary-engineering',
		'name' => 'Сантехника',
		'title' => 'Сантехника',
		'subCategories' => array(
			array(
				'urlKey' => 'bath',
				'name' => 'Ванны',
				'title' => 'Ванны',
			),
			array(
				'urlKey' => 'water-heaters',
				'name' => 'Водонагреватели',
				'title' => 'Водонагреватели',
			),
			array(
				'urlKey' => 'shower-stalls-and-boxes',
				'name' => 'Душевые кабины и боксы',
				'title' => 'Душевые кабины и боксы',
			),
			array(
				'urlKey' => 'traps-for-bath',
				'name' => 'Сифоны для ванн',
				'title' => 'Сифоны для ванн',
			),
			array(
				'urlKey' => 'curtain-for-a-buth',
				'name' => 'Шторки для ванны',
				'title' => 'Шторки для ванны',
			),
			array(
				'urlKey' => 'bath-screens',
				'name' => 'Экраны под ванну',
				'title' => 'Экраны под ванну',
			),
			array(
				'urlKey' => 'installation',
				'name' => 'Инсталяции',
				'title' => 'Инсталяции',
			),
			array(
				'urlKey' => 'toilets',
				'name' => 'Унитазы',
				'title' => 'Унитазы',
			),
			array(
				'urlKey' => 'traps-viega',
				'name' => 'Сифоны Viega',
				'title' => 'Сифоны Viega',
			),
			array(
				'urlKey' => 'thermostats',
				'name' => 'Термостаты',
				'title' => 'Термостаты',
			),
			array(
				'urlKey' => 'bathroom-faucets',
				'name' => 'Смесители для ванной',
				'title' => 'Смесители для ванной',
			),
			array(
				'urlKey' => 'bidet-faucets',
				'name' => 'Смесители для биде',
				'title' => 'Смесители для биде',
			),
			array(
				'urlKey' => 'sink',
				'name' => 'Смесители для раковины',
				'title' => 'Смесители для раковины',
			),
			array(
				'urlKey' => 'kitchen-mixers',
				'name' => 'Смесители для кухни',
				'title' => 'Смесители для кухни',
			),
			array(
				'urlKey' => 'hoses-for-a-shower',
				'name' => 'Шланги для душа',
				'title' => 'Шланги для душа',
			),
			array(
				'urlKey' => 'shower-faucets',
				'name' => 'Смесители для душа',
				'title' => 'Смесители для душа',
			),
		),
	),
	array(
		'urlKey' => 'materials',
		'name' => 'Материалы',
		'title' => 'Материалы',
		'subCategories' => array(
			array(
				'urlKey' => 'the-adhesive-mixture',
				'name' => 'Клеящие смеси',
				'title' => 'Клеящие смеси',
			),
			array(
				'urlKey' => 'primers',
				'name' => 'Грунтовки',
				'title' => 'Грунтовки',
			),
			array(
				'urlKey' => 'grout-for-joints',
				'name' => 'Затирки для швов',
				'title' => 'Затирки для швов',
			),
			array(
				'urlKey' => 'putty',
				'name' => 'Шпатлевки',
				'title' => 'Шпатлевки',
			),
			array(
				'urlKey' => 'tools-for-tratment',
				'name' => 'Средства для очистки',
				'title' => 'Средства для очистки',
			),
			array(
				'urlKey' => 'corners-for-tile',
				'name' => 'Уголки для кафеля',
				'title' => 'Уголки для кафеля',
			),
		),
	),
	array(
		'urlKey' => 'accessories',
		'name' => 'Аксессуары',
		'title' => 'Аксессуары',
		'subCategories' => array(
		),
	),
	array(
		'urlKey' => 'furniture',
		'name' => 'Мебель',
		'title' => 'Мебель',
		'subCategories' => array(
		),
	),
	array(
		'urlKey' => 'heating',
		'name' => 'Отопление',
		'title' => 'Отопление',
		'subCategories' => array(
			array(
				'urlKey' => 'heaters-for-water',
				'name' => 'Водонагреватели',
				'title' => 'Водонагреватели',
			),
			array(
				'urlKey' => 'towel-warmers',
				'name' => 'Полотенцесушители',
				'title' => 'Полотенцесушители',
			),
		),
	),
);

foreach ($rootIds as $group) {
	$parentCategory = Mage::getModel('catalog/category')
		->load($group['root_category_id']);
	$installer->addNewCategoriesFromList( $categories, $parentCategory, $group['store_id'] );
}

$installer->endSetup();
