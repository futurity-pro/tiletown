<?php

class Freaks_Products_Block_Region extends Mage_Catalog_Block_Product_Abstract
{
    const DEFAULT_PRODUCTS_COUNT = 3;

    protected
        $_productCollection,
        $_productsCount;

    protected function _construct()
    {
        $this->addData(array(
            'cache_lifetime'    => 86400,
            'cache_tags'        => array(Mage_Catalog_Model_Product::CACHE_TAG),
        ));
    }

    public function getCacheKeyInfo()
    {
        return array(
           'CATALOG_PRODUCT_VIEWS_BLOCK',
           Mage::app()->getStore()->getId(),
           Mage::getDesign()->getPackageName(),
           Mage::getDesign()->getTheme('template'),
           Mage::getSingleton('customer/session')->getCustomerGroupId(),
           'template' => $this->getTemplate(),
           $this->getProductsCount(),
           $this->getType()
        );
    }

    /**
     * Set how much product should be displayed at once.
     *
     * @param $count
     * @return Freaks_Products_Block_Region
     */
    public function setProductsCount($count)
    {
        $this->_productsCount = $count;
        return $this;
    }

    /**
     * Get how much products should be displayed at once.
     *
     * @return int
     */
    public function getProductsCount()
    {
        if (null === $this->_productsCount) {
            $this->_productsCount = self::DEFAULT_PRODUCTS_COUNT;
        }
        return $this->_productsCount;
    }

    public function getProductCollection()
    {
        $this->_productCollection = Mage::getResourceModel('catalog/product_collection')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents()
            ->addUrlRewrite($this->getCategoryId());
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($this->_productCollection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInSearchFilterToCollection($this->_productCollection);
        return $this->_productCollection;
    }

    public function getLastProducts()
    {
        return $this->getProductCollection()
            ->setOrder('entity_id', 'desc')
            ->setCurPage(1)
            ->setPageSize($this->getProductsCount());
    }

    public function getBestsellerProducts()
    {
        return $this->getProductCollection()
            // ->addOrderedQty()
            ->setOrder('ordered_qty','desc')
            ->setPageSize($this->getProductsCount());
    }

    public function getSpecialProducts()
    {
        $date = Mage::getModel('core/date');
        return $this->getProductCollection()
            ->addAttributeToSort('special_from_date','desc')
            ->addAttributeToFilter('special_from_date', array(
                'date' => true, 'to' => $date->date()
            ))
            ->addAttributeToFilter('special_to_date', array( 'or' => array(
                0 => array('date' => true, 'from' => $date->timestamp() + 86400), // tomorrow date
                1 => array('is'   => new Zend_Db_Expr('null')))
            ), 'left')
            ->setCurPage(1)
            ->setPageSize($this->getProductsCount());
    }

    public function getNewProducts()
    {
        $todayDate  = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

        return $this->getProductCollection()
            ->addAttributeToFilter('news_from_date', array('or' => array(
                0 => array('date' => true, 'to' => $todayDate),
                1 => array('is' => new Zend_Db_Expr('null')))
            ), 'left')
            ->addAttributeToFilter('news_to_date', array('or' => array(
                0 => array('date' => true, 'from' => $todayDate),
                1 => array('is' => new Zend_Db_Expr('null')))
            ), 'left')
            ->addAttributeToFilter(array(
                array('attribute' => 'news_from_date', 'is' => new Zend_Db_Expr('not null')),
                array('attribute' => 'news_to_date', 'is'   => new Zend_Db_Expr('not null'))
            ))
            ->addAttributeToSort('news_from_date', 'desc')
            ->setPageSize($this->getProductsCount())
            ->setCurPage(1);
    }

    public function getCategoryId()
    {
        return Mage::app()->getStore()->getRootCategoryId();
    }

    public function getFlaggedProducts($flagName, $flagValue = 1)
    {
        return $this->getProductCollection()
            ->addAttributeToFilter($flagName, $flagValue)
            ->addAttributeToSort($this->getSortBy(), 'desc');
    }

    public function getFlaggedSortBy()
    {
        $sortBy = $this->getData('flagged_sort_by');
        if (!$sortBy) {
            $sortBy = 'entity_id';
        }
        return $sortBy;
    }

    public function getProducts()
    {
        $type = $this->getType();
        if (!$type) {
            Mage::throwException($this->__('Products block type must be specified'));
        }

        $method = 'get' . ucfirst(strtolower($type)) . 'Products';
        if (!method_exists($this, $method)) {
            Mage::throwException($this->__('Unknown products block type'));
        }

        return $this->$method();
    }
}